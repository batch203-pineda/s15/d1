console.log("Hello, wolrd!");
console. log ("Hello, Batch 203!");
console. log (
    "hello guys!"
);



// Comments:

// This is a single line comment

/*
    This is a multi line comment
*/

// Syntax and Statements

/* Statements - in programing are instruction that we tell the
computer to perform */

/* Syntax - in programming, it is the set of rules that describes how statements must be constructed */

/* Variables - it is used to contain data

    - Syntax in declaring in variables (camel case)
        - let/const variableNameSample;
        - let: can change the assigned value
        - const: can't be change the assigned value
*/

let myVariable = "Hello";

console.log(myVariable);

/* console.log(hello);

let hello; */

/* 
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator(=) to assign a value/
        2. Variable name should start with a lowercase character, use camelCase for the multiple words.
        3. For constant variables, use const

        
*/

/* Declaring a variable with initial value
    let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let product = 'Alvin\'s Computer';
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// let - we usually change/reassign the values in our variable

// Reassigning variable values
// Syntax
    // variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";

console.log(friend); //error: Identifier "friend" has already been declared.

//interest = 4.489;
console.log(interest); // error because of assigning a value to a constant variable.

// Re-assigning variables vs initializing variables.

let supplier;
//  Initialization is done after the variables has been declared.
supplier = "Jogn Smith Tradings";
console.log(supplier);

//  Reassignment of variable because its initial value was already declared.
supplier = "Zuitt Store";
console.log(supplier);

// NOTE: dapat may value n agad pag kay const
const pi = 3.1416;
// pi = 3.1416;
console.log(pi);

/* 
    var(ES1) vs let/const(ES6)

    let/const keyword avoid the hoisting of variables.

a = 5;
console.log(a);
var a;

*/


// Multiple variable declarations
let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

/* Using a variable with a reserved keyword.

const let = "hello";
console.log(let); //error: cannot use reserved keyword as a variable name.

*/


/* Data Types

   Data Types and Variables

    undefined, null, boolean, string, symbol, number and object

    undefined - the undefined property indicates that a variable has not been assigned a value or not declared at all.

    null - means nothing, you set it to be something and that thing is nothing.

    boolean (bool)- used where data is restricted with True/False or Yes/No options. 


    string (str or text)- a string is a combination of any character that appear on a keyboard such as letters, numbers or symbols.

    e.g. var sayHello = “Hello World! This is my 1st code”;

    symbol - is an immutable primitive value that is unique


    number (int or integer) - any whole number

    object - can store a lot of key value pairs 


*/
let country = "Philippines";

let province = "Manila";


// Concatenation Strings
// Multiple string values can be combined to create a single string using the "+" symbol.
let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape character "\"
// "\n" refers to create new line or set the text to text line.
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/ Whole Number
let headcount = 26;
console.log(headcount);

// Decimal numbers/fraction
let grade = 98.7;
console.log(grade);

// Exponential Notation
let plantDistance = 2e10;
console.log(plantDistance);

// Combine number and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// true/false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);





// Arrays 
// It is used to store multiple values with similar data type.
/* e.g.
    let/const arrayName = [elementA, elementB, elementC, ]
    
*/

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
// Storing different data types inside an array is not recommended becuase it will not make sense to in the context of prgramming.
let details = ["John", "Smith", 32, true  ]
console.log(details);


/* 
    Objects

    Objects are another special kind of data type that's use to mimic real world objects/items

    e.g.

        let/const objectName = {
            propertyA: value,
            propertyB: value
        }
*/


let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["0906 123 4567", "8123 4567"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
}

console.log(person);


/* 

    Create Abstract Object
*/

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6
}

console.log(myGrades);

// typeof operator is used to determine the type of data or the variable value
console.log(typeof myGrades);

console.log(typeof grades);



// Constant Objects and Arrays
// We cannot reassign the value of the variable but we can change the element of the constant array.
const anime = ["one piece", "one punch man", "attack on titan"];
anime[0] ="kimetsu no yaiba";
console.log(anime);



/*  Null

    It is used to intentionally express the absence of the value in a variable declaratio/initialization.

*/

let spouse = null;
console.log(spouse);

let myNumber = 0; //number
let myString = ""; // string


// Undefined
// Represents the state of a varaible that has been declared but without an assigned value.
let fullName;
console.log(fullName);